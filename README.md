autostream
==========

A basic wrapper module for the [Monkey programming language](https://github.com/blitz-research/monkey), which provides target appropriate 'Stream' objects for basic file I/O (Only works 100% on some targets).
